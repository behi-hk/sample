import { forwardRef, Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { JwtModule } from '@nestjs/jwt'
import { PassportModule } from '@nestjs/passport'

import { DeliveryModule } from '../delivery/delivery.module'
import { AuthController } from './auth.controller'
import { LocalStrategy } from './strategies/local.strategy'

@Module({
  controllers: [AuthController],
  providers: [LocalStrategy],
  exports: [],
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    ConfigModule.forRoot(),
    JwtModule.register({
      secret: process.env.JWT_SECRET_KEY,
      signOptions: { expiresIn: process.env.JWT_EXPIRE_TIME },
    }),
    forwardRef(() => DeliveryModule),
  ],
})
export class AuthModule {}
