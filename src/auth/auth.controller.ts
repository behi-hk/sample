import { Request } from 'express'

import { Controller, Post, Req, UseGuards } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'

import { LocalAuthGuard } from './guards/local-auth.guard'

@Controller('auth')
export class AuthController {
  constructor(private readonly jwtService: JwtService) {}

  /*
  body: {
    username: string , 
    password: string
  } 
  */
  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Req() req: Request) {
    const payload = {
      deliveryId: (req.user as any).id, 
    }
    return {
      token: this.jwtService.sign(payload)
    }
  }

  
}
