

import { NestFactory } from '@nestjs/core'
import { WsAdapter } from '@nestjs/platform-ws'

import { AppModule } from './app.module'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)

  // WS
  app.useWebSocketAdapter(new WsAdapter(app))

  app.listen(process.env.SERVICE_HTTP_PORT).then(() => {
    console.log(`Server started - http://127.0.0.1:${process.env.SERVICE_HTTP_PORT}`)
  })
}
bootstrap()
