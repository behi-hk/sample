import { forwardRef, Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'

import { AuthModule } from '../auth/auth.module'
import { DeliveryService } from './delivery.service'
import { DeliveryGateway } from './events/delivery.gateway'

@Module({
  imports: [
    ConfigModule.forRoot(),
    forwardRef(() => AuthModule),
    
  ],
  controllers: [],
  providers: [DeliveryService, DeliveryGateway],
  exports: [],
})
export class DeliveryModule {}
