import { WsStateEnum } from '../enums/ws-state.enum'

export class DeliveryData {
  deliveryId: string
  location: {
    lat: number
    lng: number
  }
  wsState: WsStateEnum
  
}
