
import * as redis from 'redis'

import { forwardRef, Inject, Injectable } from '@nestjs/common'

import { DeliveryGateway } from './events/delivery.gateway'
import { DeliveryData } from './models/delivery-data.model'

@Injectable()
export class DeliveryService {
  constructor(
    @Inject(forwardRef(() => DeliveryGateway))
    private readonly deliveryGateway: DeliveryGateway,
  ) {}

  geoClient = redis.createClient()


 async findNearDeliveriesAndSayHi(deliveryData:DeliveryData){
  let nearDeliveryIds = []
    this.geoClient.georadius(
      'locations',
      deliveryData.location.lng,
      deliveryData.location.lat,
      10,
      'km',
      'WITHCOORD',
      'WITHDIST',
      (err: any, result: any) => {
        nearDeliveryIds = result && result.map((item: any) => item[0])

        this.deliveryGateway.clients.forEach((client: any) => {
          if (
            nearDeliveryIds.includes(client.delivery.deliveryId) && client.delivery.deliveryId != deliveryData.deliveryId
          ) {
            client.send(JSON.stringify('Hi buddy!'))
          }
        })
      },
      
    )
    
    
    
 }

  async updateLocationsInRedis(lat: number, lng: number, deliveryId: string) {
    this.geoClient.geoadd('locations', lng, lat, deliveryId, (err: any, result: any) => {
      if (err) throw new Error('update location failed!')
      
    })
  }

 
}


