import { config } from 'dotenv'
import * as jwt from 'jsonwebtoken'
import { parse } from 'url'
import WebSocket, { Server } from 'ws'

import { forwardRef, Inject } from '@nestjs/common'
import {
    OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit, SubscribeMessage, WebSocketGateway,
    WebSocketServer
} from '@nestjs/websockets'

import { DeliveryService } from '../delivery.service'
import { UpdateDeliveryStateDto } from '../dto/delivery-state.dto'
import { WsStateEnum } from '../enums/ws-state.enum'
import { DeliveryData } from '../models/delivery-data.model'

config();
@WebSocketGateway(Number(process.env.SERVICE_WS_DELIVERY_PORT))
export class DeliveryGateway
  implements OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit {
  constructor(
    @Inject(forwardRef(() => DeliveryService))
    protected readonly deliveryService: DeliveryService
  ) {}

  clients: Map<string, WebSocket> = new Map();

  @WebSocketServer()
  server: Server;

  public afterInit(server: Server) {
    console.log(`ws connected on port ${process.env.SERVICE_WS_DELIVERY_PORT}`);
  }
  private async validateUserConnection(req: Request): Promise<DeliveryData> {
    let authorization: string = null;
    if (req.headers["authorization"]) {
      authorization = req.headers["authorization"].split("Bearer ")[1];
    } else if (parse(req.url).query.split("=")[0] == "token") {
      authorization = parse(req.url).query.split("=")[1];
    }

    let payload: any;
    try {
      payload = jwt.verify(authorization, process.env.JWT_SECRET_KEY);
    } catch (error) {}

    if (payload && payload.deliveryId) {
      const deliveryData = new DeliveryData();
      deliveryData.deliveryId = payload.deliveryId;
      deliveryData.wsState = WsStateEnum.CONNECTED;
      return deliveryData;
    }

    throw new Error("User is not found!");
  }

  public async handleConnection(client: WebSocket, req: Request) {
    try {
      const deliveryData = await this.validateUserConnection(req);
      (client as any).delivery = deliveryData;

      this.clients.set(deliveryData.deliveryId, client);
      client.send(JSON.stringify("CONNECTED!"));
    } catch (error) {
      client.send(error.message);
      client.close();
    }
  }

  public async handleDisconnect(client: WebSocket) {
    const deliveryId = (client as any).delivery.deliveryId;
    this.clients.delete(deliveryId);
  }

  /**
   * message body example: 
  *{
    "event":"UPDATE_LOCATION",
    "data":{
      "location":{
        "lat":"32.21345",
        "lng":"54.21212"
    }
  }
  }
   */
  @SubscribeMessage("UPDATE_LOCATION")
  async updateAction(client: WebSocket, data: UpdateDeliveryStateDto) {
    (client as any).delivery.location = data.location;

    await this.deliveryService.updateLocationsInRedis(
      data.location.lat,
      data.location.lng,
      (client as any).delivery.deliveryId
    );
    client.send("LOCATION UPDATED");
  }

    /**
   * message body example: 
  *{
    "event":"SAY_HI"
    
  }
  }
   */
  @SubscribeMessage("SAY_HI")
  async sayHiToNearDeliveries(client: WebSocket) {
    await this.deliveryService.findNearDeliveriesAndSayHi((client as any).delivery)
  }
}
