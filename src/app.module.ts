

import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'

import { AuthModule } from './auth/auth.module'
import { DeliveryModule } from './delivery/delivery.module'

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    DeliveryModule,
    AuthModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
